# Recommender

This program is using [Glide](https://github.com/Masterminds/glide) for dependency vendoring, and [Gox](https://github.com/mitchellh/gox) for cross compiling.

## Building this project

To build this project as I originally did, simply install [Glide](https://github.com/Masterminds/glide) and [Gox](https://github.com/mitchellh/gox), and then issue the following commands:

    glide install
    gox -os="linux darwin windows" -arch="386 amd64 arm"

You should now see a bunch of `recommender_*` designated files for the various architectures and os's you just built.
