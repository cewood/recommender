package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"
)

type sortedMapStringInt struct {
	// Actual data, keyed on product, and counting occurences
	data map[string]int
	// Sorting reference, slice of keys in order of count
	sort []string
}

func (s *sortedMapStringInt) Len() int {
	return len(s.data)
}

func (s *sortedMapStringInt) Less(i, j int) bool {
	return s.data[s.sort[i]] > s.data[s.sort[j]]
}

func (s *sortedMapStringInt) Swap(i, j int) {
	s.sort[i], s.sort[j] = s.sort[j], s.sort[i]
}

func (s *sortedMapStringInt) InitSort() {
	// Initialise the sort file of our data object
	s.sort = make([]string, len(s.data))

	i := 0
	for key, _ := range s.data {
		s.sort[i] = key
		i++
	}
}

func trace(debug bool, s string) (bool, string, time.Time) {
	if debug {
		log.Println("START: ", s)
	}
	return debug, s, time.Now()
}

func un(debug bool, s string, startTime time.Time) {
	if debug {
		endTime := time.Now()
		log.Println("  END: ", s, ", ElapsedTime in seconds:", endTime.Sub(startTime))
	}
}

func readCsvFile(debug bool, file string) (*os.File, *csv.Reader) {
	defer un(trace(debug, "readCsvFile()"))

	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	return f, csv.NewReader(f)
}

func parseFile(debug bool, file string) (map[string]map[string]struct{}, map[string]map[string]struct{}, sortedMapStringInt) {
	defer un(trace(debug, "parseFile()"))

	// Open our file for reading
	fileHandle, csvReader := readCsvFile(debug, file)

	// Make sure we close the file afterwards
	defer fileHandle.Close()

	// Initialise our data stores
	productsByCustomerId := make(map[string]map[string]struct{})
	customersByProductId := make(map[string]map[string]struct{})
	countByProductId := new(sortedMapStringInt)
	countByProductId.data = make(map[string]int)

	// Iterate over our file
	for {
		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		// Split the record from reader into fields
		recordSplit := strings.Split(record[0], ";")
		thisCustomerId := recordSplit[1]
		thisProductId := recordSplit[2]

		// Initalise our inner maps if they don't already exist
		if _, ok := productsByCustomerId[thisCustomerId]; !ok {
			productsByCustomerId[thisCustomerId] = make(map[string]struct{})
		}
		if _, ok := customersByProductId[thisProductId]; !ok {
			customersByProductId[thisProductId] = make(map[string]struct{})
		}
		if _, ok := productsByCustomerId[thisCustomerId][thisProductId]; !ok {
			productsByCustomerId[thisCustomerId][thisProductId] = struct{}{}
		}
		if _, ok := customersByProductId[thisProductId][thisCustomerId]; !ok {
			customersByProductId[thisProductId][thisCustomerId] = struct{}{}
		}
		countByProductId.data[thisProductId] += 1
	}

	// Time to prepare and sort our data
	countByProductId.InitSort()
	sort.Sort(countByProductId)

	return productsByCustomerId, customersByProductId, *countByProductId
}

func recommendByTotalCount(debug bool, userId string, count int, productsByCustomerId map[string]map[string]struct{}, countByProductId sortedMapStringInt) []string {
	defer un(trace(debug, "recommendByTotalCount()"))

	if debug {
		fmt.Printf("purchase history of user '%v' is: %v\n", userId, productsByCustomerId[userId])
	}

	seenCount := 0
	suggest := make([]string, 0)
	for key := range countByProductId.sort {
		if debug {
			fmt.Printf("validating product '%v': ", countByProductId.sort[key])
		}

		if _, ok := productsByCustomerId[userId][countByProductId.sort[key]]; !ok {
			suggest = append(suggest, countByProductId.sort[key])
			if debug {
				fmt.Printf("accepted\n")
			}
		} else {
			if debug {
				fmt.Printf("rejected\n")
			}
		}
		seenCount++
		if len(suggest) == count {
			break
		}
	}

	return suggest
}

func recommend(debug bool, count int, file, rtype, user string) {
	defer un(trace(debug, "recommend()"))

	// Retrieve the data parsed from csv
	productsByCustomerId, _, countByProductId := parseFile(debug, file)

	// Check the supplied user actually exists
	if _, ok := productsByCustomerId[user]; !ok {
		log.Fatalf("Supplied user doesn't exist: %v, exiting.\n", user)
	}

	switch rtype {
	case "total_count":
		recommendations := recommendByTotalCount(debug, user, count, productsByCustomerId, countByProductId)
		fmt.Printf("recommendations for '%v' are: %v\n", user, recommendations)
	default:
		fmt.Printf("There's currently no recommender named '%v', exiting.\n", rtype)
		return
	}
}

func main() {
	var (
		recommendDebug = kingpin.Flag("debug", "Enable debug mode.").OverrideDefaultFromEnvar("RECOMMEND_DEBUG").Bool()
		recommendCount = kingpin.Flag("count", "The number of recommendations to retrieve").Default("5").OverrideDefaultFromEnvar("RECOMMEND_COUNT").Int()
		recommendFile  = kingpin.Flag("file", "File to load data from").Default("purchases.csv").OverrideDefaultFromEnvar("RECOMMEND_FILE").ExistingFile()
		recommendType  = kingpin.Flag("type", "The type of recommendation to make").Default("total_count").OverrideDefaultFromEnvar("RECOMMEND_TYPE").String()
		recommendUser  = kingpin.Flag("user", "User id to generate recommendations for").Default("845641").OverrideDefaultFromEnvar("RECOMMEND_USER").String()
	)
	kingpin.Parse()

	defer un(trace(*recommendDebug, "main()"))
	recommend(*recommendDebug, *recommendCount, *recommendFile, *recommendType, *recommendUser)
}
